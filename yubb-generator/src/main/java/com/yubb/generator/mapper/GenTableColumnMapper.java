package com.yubb.generator.mapper;

import java.util.List;

import com.baomidou.mybatisplus.annotation.InterceptorIgnore;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yubb.generator.domain.GenTableColumn;
import com.yubb.generator.domain.dto.GenTableColumnDTO;
import com.yubb.generator.domain.vo.GenTableColumnVO;
import org.apache.ibatis.annotations.Mapper;

/**
 *@Description 业务字段 数据层
 *@Author zhushuyong
 *@Date 2021/6/23 17:49
 *@since:
 *@copyright: 版权所有2021 开源组织 gitee(https://gitee.com/jinzheyi)作者：朱述勇<br/>
 *            GitHub(https://github.com/jinzheyi)作者：朱述勇 。
 */
@Mapper
@InterceptorIgnore(tenantLine = "true")
public interface GenTableColumnMapper extends BaseMapper<GenTableColumn> {
    /**
     * 根据表名称查询列信息
     * 
     * @param tableName 表名称
     * @return 列信息
     */
    public List<GenTableColumnVO> selectDbTableColumnsByName(String tableName);

    /**
     * 查询业务字段列表
     * 
     * @param genTableColumn 业务字段信息
     * @return 业务字段集合
     */
    public List<GenTableColumnVO> selectGenTableColumnListByTableId(GenTableColumnDTO genTableColumn);

}
