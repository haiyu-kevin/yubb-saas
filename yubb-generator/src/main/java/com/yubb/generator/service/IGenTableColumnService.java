package com.yubb.generator.service;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yubb.generator.domain.GenTableColumn;
import com.yubb.generator.domain.dto.GenTableColumnDTO;
import com.yubb.generator.domain.vo.GenTableColumnVO;

/**
 *@Description 业务字段 服务层
 *@Author zhushuyong
 *@Date 2021/6/23 18:02
 *@since:
 *@copyright: 版权所有2021 开源组织 gitee(https://gitee.com/jinzheyi)作者：朱述勇<br/>
 *            GitHub(https://github.com/jinzheyi)作者：朱述勇 。
 */
public interface IGenTableColumnService extends IService<GenTableColumn>
{
    /**
     * 查询业务字段列表
     * 
     * @param genTableColumn 业务字段信息
     * @return 业务字段集合
     */
    public List<GenTableColumnVO> selectGenTableColumnListByTableId(GenTableColumnDTO genTableColumn);

    /**
     * 新增业务字段
     * 
     * @param genTableColumn 业务字段信息
     * @return 结果
     */
    public int insertGenTableColumn(GenTableColumnDTO genTableColumn);

    /**
     * 修改业务字段
     * 
     * @param genTableColumn 业务字段信息
     * @return 结果
     */
    public int updateGenTableColumn(GenTableColumnDTO genTableColumn);

    /**
     * 删除业务字段信息
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteGenTableColumnByIds(String ids);
}
