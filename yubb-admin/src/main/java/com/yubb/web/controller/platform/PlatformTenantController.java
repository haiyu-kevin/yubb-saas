package com.yubb.web.controller.platform;

import com.yubb.common.annotation.PlatformLog;
import com.yubb.common.core.controller.BaseController;
import com.yubb.common.core.domain.AjaxResult;
import com.yubb.common.core.domain.platform.dto.PlatformTenantDTO;
import com.yubb.common.core.domain.platform.vo.PlatformTenantVO;
import com.yubb.common.core.page.TableDataInfo;
import com.yubb.common.enums.BusinessType;
import com.yubb.common.utils.ShiroUtils;
import com.yubb.common.utils.poi.ExcelUtil;
import com.yubb.framework.shiro.service.SysPasswordService;
import com.yubb.platform.service.IPlatformTenantService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 租户Controller
 * 
 * @author zhushuyong
 * @date 2021-07-02
 */
@Controller
@RequestMapping("/platform/tenant")
public class PlatformTenantController extends BaseController
{
    private String prefix = "platform/tenant";

    @Autowired
    private IPlatformTenantService platformTenantService;

    @Autowired
    private SysPasswordService passwordService;

    @RequiresPermissions("platform:tenant:view")
    @GetMapping()
    public String tenant()
    {
        return prefix + "/tenant";
    }

    /**
     * 查询租户列表
     */
    @RequiresPermissions("platform:tenant:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo<PlatformTenantVO> list(PlatformTenantDTO platformTenant)
    {
        return platformTenantService.selectPlatformTenantPage(platformTenant);
    }

    /**
     * 导出租户列表
     */
    @RequiresPermissions("platform:tenant:export")
    @PlatformLog(title = "租户", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(PlatformTenantDTO platformTenant)
    {
        List<PlatformTenantVO> list = platformTenantService.selectPlatformTenantList(platformTenant);
        ExcelUtil<PlatformTenantVO> util = new ExcelUtil<PlatformTenantVO>(PlatformTenantVO.class);
        return util.exportExcel(list, "租户列表数据");
    }

    /**
     * 新增租户
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存租户
     */
    @RequiresPermissions("platform:tenant:add")
    @PlatformLog(title = "租户", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(@Validated PlatformTenantDTO platformTenant)
    {
        platformTenant.setSalt(ShiroUtils.randomSalt());
        platformTenant.setPassword(passwordService.encryptPassword(platformTenant.getLoginName(), platformTenant.getPassword(), platformTenant.getSalt()));
        platformTenant.setCreateBy(ShiroUtils.getPlatformUser().getLoginName());
        return toAjax(platformTenantService.insertPlatformTenant(platformTenant));
    }

    /**
     * 修改租户
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") String id, ModelMap mmap)
    {
        PlatformTenantVO platformTenant = platformTenantService.selectPlatformTenantById(id);
        mmap.put("platformTenant", platformTenant);
        return prefix + "/edit";
    }

    /**
     * 修改保存租户
     */
    @RequiresPermissions("platform:tenant:edit")
    @PlatformLog(title = "租户", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(PlatformTenantDTO platformTenant)
    {
        return toAjax(platformTenantService.updatePlatformTenant(platformTenant));
    }

    /**
     * 校验租户编号是否唯一
     */
    @PostMapping("/checkTenantNoUnique")
    @ResponseBody
    public String checkTenantNoUnique(PlatformTenantDTO platformTenantDTO)
    {
        return platformTenantService.checkTenantNoUnique(platformTenantDTO);
    }

    /**
     * 租户状态修改
     */
    @PlatformLog(title = "租户管理", businessType = BusinessType.UPDATE)
    @RequiresPermissions("platform:tenant:edit")
    @PostMapping("/changeStatus")
    @ResponseBody
    public AjaxResult changeStatus(PlatformTenantDTO platformTenantDTO) {
        return toAjax(platformTenantService.changeStatus(platformTenantDTO));
    }
}
