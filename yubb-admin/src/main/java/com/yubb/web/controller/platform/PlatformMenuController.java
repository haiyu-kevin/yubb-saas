package com.yubb.web.controller.platform;

import com.yubb.common.annotation.PlatformLog;
import com.yubb.common.constant.Constants;
import com.yubb.common.constant.UserConstants;
import com.yubb.common.core.controller.BaseController;
import com.yubb.common.core.domain.AjaxResult;
import com.yubb.common.core.domain.Ztree;
import com.yubb.common.core.domain.platform.dto.PlatformMenuDTO;
import com.yubb.common.core.domain.platform.dto.PlatformRoleDTO;
import com.yubb.common.core.domain.platform.vo.PlatformMenuVO;
import com.yubb.common.enums.BusinessType;
import com.yubb.common.utils.ShiroUtils;
import com.yubb.framework.shiro.util.AuthorizationUtils;
import com.yubb.platform.service.IPlatformMenuService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 *@Description 菜单信息
 *@Author zhushuyong
 *@Date 2021/5/26 14:55
 *@since:
 *@copyright: 版权所有2021 开源组织 gitee(https://gitee.com/jinzheyi)作者：朱述勇<br/>
 *            GitHub(https://github.com/jinzheyi)作者：朱述勇 。
 */
@Controller
@RequestMapping("/platform/menu")
public class PlatformMenuController extends BaseController
{
    private String prefix = "platform/menu";

    @Autowired
    private IPlatformMenuService menuService;

    @RequiresPermissions("platform:menu:view")
    @GetMapping()
    public String menu()
    {
        return prefix + "/menu";
    }

    @RequiresPermissions("platform:menu:list")
    @PostMapping("/list")
    @ResponseBody
    public List<PlatformMenuVO> list(PlatformMenuDTO menu)
    {
        List<PlatformMenuVO> menuList = menuService.selectMenuList(menu,
                ShiroUtils.getPlatformUser().getId());
        return menuList;
    }

    /**
     * 删除菜单
     */
    @PlatformLog(title = "菜单管理", businessType = BusinessType.DELETE)
    @RequiresPermissions("platform:menu:remove")
    @GetMapping("/remove/{id}")
    @ResponseBody
    public AjaxResult remove(@PathVariable("id") String id)
    {
        if (menuService.selectCountMenuByParentId(id) > 0)
        {
            return AjaxResult.warn("存在子菜单,不允许删除");
        }
        if (menuService.selectCountRoleMenuByMenuId(id) > 0)
        {
            return AjaxResult.warn("菜单已分配,不允许删除");
        }
        AuthorizationUtils.clearAllCachedAuthorizationInfo();
        return toAjax(menuService.deleteMenuById(id));
    }

    /**
     * 新增
     */
    @GetMapping("/add/{parentId}")
    public String add(@PathVariable("parentId") String parentId, ModelMap mmap)
    {
        PlatformMenuVO menu = null;
        if (!Constants.TOP_PARENT_MENU_ID.equals(parentId))
        {
            menu = menuService.selectMenuById(parentId);
        }
        else
        {
            menu = new PlatformMenuVO();
            menu.setId(Constants.TOP_PARENT_MENU_ID);
            menu.setMenuName("主目录");
        }
        mmap.put("menu", menu);
        return prefix + "/add";
    }

    /**
     * 新增保存菜单
     */
    @PlatformLog(title = "菜单管理", businessType = BusinessType.INSERT)
    @RequiresPermissions("platform:menu:add")
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(@Validated PlatformMenuDTO menu)
    {
        if (UserConstants.MENU_NAME_NOT_UNIQUE.equals(menuService.checkMenuNameUnique(menu)))
        {
            return error("新增菜单'" + menu.getMenuName() + "'失败，菜单名称已存在");
        }
        menu.setCreateBy(ShiroUtils.getPlatformUser().getLoginName());
        AuthorizationUtils.clearAllCachedAuthorizationInfo();
        return toAjax(menuService.insertMenu(menu));
    }

    /**
     * 修改菜单
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") String id, ModelMap mmap)
    {
        mmap.put("menu", menuService.selectMenuById(id));
        return prefix + "/edit";
    }

    /**
     * 修改保存菜单
     */
    @PlatformLog(title = "菜单管理", businessType = BusinessType.UPDATE)
    @RequiresPermissions("platform:menu:edit")
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(@Validated PlatformMenuDTO menu)
    {
        if (UserConstants.MENU_NAME_NOT_UNIQUE.equals(menuService.checkMenuNameUnique(menu)))
        {
            return error("修改菜单'" + menu.getMenuName() + "'失败，菜单名称已存在");
        }
        menu.setUpdateBy(ShiroUtils.getPlatformUser().getLoginName());
        AuthorizationUtils.clearAllCachedAuthorizationInfo();
        return toAjax(menuService.updateMenu(menu));
    }

    /**
     * 选择菜单图标
     */
    @GetMapping("/icon")
    public String icon()
    {
        return prefix + "/icon";
    }

    /**
     * 校验菜单名称
     */
    @PostMapping("/checkMenuNameUnique")
    @ResponseBody
    public String checkMenuNameUnique(PlatformMenuDTO menu)
    {
        return menuService.checkMenuNameUnique(menu);
    }

    /**
     * 加载角色菜单列表树
     */
    @GetMapping("/roleMenuTreeData")
    @ResponseBody
    public List<Ztree> roleMenuTreeData(PlatformRoleDTO role)
    {
        List<Ztree> ztrees = menuService.roleMenuTreeData(role, ShiroUtils.getPlatformUser().getId());
        return ztrees;
    }

    /**
     * 加载所有菜单列表树
     */
    @GetMapping("/menuTreeData")
    @ResponseBody
    public List<Ztree> menuTreeData()
    {
        List<Ztree> ztrees = menuService.menuTreeData(ShiroUtils.getPlatformUser().getId());
        return ztrees;
    }

    /**
     * 选择菜单树
     */
    @GetMapping("/selectMenuTree/{id}")
    public String selectMenuTree(@PathVariable("id") String id, ModelMap mmap)
    {
        mmap.put("menu", menuService.selectMenuById(id));
        return prefix + "/tree";
    }
}