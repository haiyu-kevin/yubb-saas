package com.yubb.web.controller.platform;

import com.yubb.common.annotation.PlatformLog;
import com.yubb.common.config.YuBbConfig;
import com.yubb.common.constant.UserConstants;
import com.yubb.common.core.controller.BaseController;
import com.yubb.common.core.domain.AjaxResult;
import com.yubb.common.core.domain.platform.dto.PlatformUserDTO;
import com.yubb.common.core.domain.platform.vo.PlatformUserVO;
import com.yubb.common.enums.BusinessType;
import com.yubb.common.utils.DateUtils;
import com.yubb.common.utils.ShiroUtils;
import com.yubb.common.utils.StringUtils;
import com.yubb.common.utils.bean.DozerUtils;
import com.yubb.common.utils.file.FileUploadUtils;
import com.yubb.framework.shiro.service.SysPasswordService;
import com.yubb.platform.service.IPlatformUserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

/**
 *@Description 个人信息 业务处理
 *@Author zhushuyong
 *@Date 2021/5/26 14:43
 *@since:
 *@copyright: 版权所有2021 开源组织 gitee(https://gitee.com/jinzheyi)作者：朱述勇<br/>
 *            GitHub(https://github.com/jinzheyi)作者：朱述勇 。
 */
@Controller
@RequestMapping("/platform/user/profile")
public class PlatformProfileController extends BaseController
{
    private static final Logger log = LoggerFactory.getLogger(PlatformProfileController.class);

    private String prefix = "platform/user/profile";

    @Autowired
    private IPlatformUserService userService;
    
    @Autowired
    private SysPasswordService passwordService;

    /**
     * 个人信息
     */
    @GetMapping()
    public String profile(ModelMap mmap)
    {
        PlatformUserVO user = ShiroUtils.getPlatformUser();
        mmap.put("user", user);
        mmap.put("roleGroup", userService.selectUserRoleGroup(user.getId()));
        mmap.put("postGroup", userService.selectUserPostGroup(user.getId()));
        return prefix + "/profile";
    }

    @GetMapping("/checkPassword")
    @ResponseBody
    public boolean checkPassword(String password)
    {
        PlatformUserVO user = ShiroUtils.getPlatformUser();
        if (passwordService.matches(user, password))
        {
            return true;
        }
        return false;
    }

    @GetMapping("/resetPwd")
    public String resetPwd(ModelMap mmap)
    {
        PlatformUserVO user = ShiroUtils.getPlatformUser();
        mmap.put("user", userService.selectUserById(user.getId()));
        return prefix + "/resetPwd";
    }

    @PlatformLog(title = "重置密码", businessType = BusinessType.UPDATE)
    @PostMapping("/resetPwd")
    @ResponseBody
    public AjaxResult resetPwd(String oldPassword, String newPassword)
    {
        PlatformUserVO user = ShiroUtils.getPlatformUser();
        if (!passwordService.matches(user, oldPassword))
        {
            return error("修改密码失败，旧密码错误");
        }
        if (passwordService.matches(user, newPassword))
        {
            return error("新密码不能与旧密码相同");
        }
        user.setSalt(ShiroUtils.randomSalt());
        user.setPassword(passwordService.encryptPassword(user.getLoginName(), newPassword, user.getSalt()));
        user.setPwdUpdateDate(DateUtils.getNowDate());
        if (userService.resetUserPwd(DozerUtils.copyProperties(user, PlatformUserDTO.class)) > 0)
        {
            ShiroUtils.setPlatformUser(user);
            return success();
        }
        return error("修改密码异常，请联系管理员");
    }

    /**
     * 修改用户
     */
    @GetMapping("/edit")
    public String edit(ModelMap mmap)
    {
        PlatformUserVO user = ShiroUtils.getPlatformUser();
        mmap.put("user", userService.selectUserById(user.getId()));
        return prefix + "/edit";
    }

    /**
     * 修改头像
     */
    @GetMapping("/avatar")
    public String avatar(ModelMap mmap)
    {
        PlatformUserVO user = ShiroUtils.getPlatformUser();
        mmap.put("user", userService.selectUserById(user.getId()));
        return prefix + "/avatar";
    }

    /**
     * 修改用户
     */
    @PlatformLog(title = "个人信息", businessType = BusinessType.UPDATE)
    @PostMapping("/update")
    @ResponseBody
    public AjaxResult update(PlatformUserDTO user)
    {
        PlatformUserVO currentUser = ShiroUtils.getPlatformUser();
        currentUser.setUserName(user.getUserName());
        currentUser.setEmail(user.getEmail());
        currentUser.setPhonenumber(user.getPhonenumber());
        currentUser.setSex(user.getSex());
        if (StringUtils.isNotEmpty(user.getPhonenumber())
                && UserConstants.USER_PHONE_NOT_UNIQUE
                .equals(userService.checkPhoneUnique(DozerUtils.copyProperties(currentUser, PlatformUserDTO.class))))
        {
            return error("修改用户'" + currentUser.getLoginName() + "'失败，手机号码已存在");
        }
        else if (StringUtils.isNotEmpty(user.getEmail())
                && UserConstants.USER_EMAIL_NOT_UNIQUE
                .equals(userService.checkEmailUnique(DozerUtils.copyProperties(currentUser, PlatformUserDTO.class))))
        {
            return error("修改用户'" + currentUser.getLoginName() + "'失败，邮箱账号已存在");
        }
        if (userService.updateUserInfo(DozerUtils.copyProperties(currentUser, PlatformUserDTO.class)) > 0)
        {
            ShiroUtils.setPlatformUser(currentUser);
            return success();
        }
        return error();
    }

    /**
     * 保存头像
     */
    @PlatformLog(title = "个人信息", businessType = BusinessType.UPDATE)
    @PostMapping("/updateAvatar")
    @ResponseBody
    public AjaxResult updateAvatar(@RequestParam("avatarfile") MultipartFile file)
    {
        PlatformUserVO currentUser = ShiroUtils.getPlatformUser();
        try
        {
            if (!file.isEmpty())
            {
                String avatar = FileUploadUtils.upload(YuBbConfig.getAvatarPath(), file);
                currentUser.setAvatar(avatar);
                if (userService.updateUserInfo(DozerUtils.copyProperties(currentUser, PlatformUserDTO.class)) > 0)
                {
                    ShiroUtils.setPlatformUser(currentUser);
                    return success();
                }
            }
            return error();
        }
        catch (Exception e)
        {
            log.error("修改头像失败！", e);
            return error(e.getMessage());
        }
    }
}
