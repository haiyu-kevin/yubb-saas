package com.yubb.web.controller.system;

import com.yubb.common.core.controller.BaseController;
import com.yubb.common.core.domain.Ztree;
import com.yubb.common.core.domain.saas.dto.SysRoleDTO;
import com.yubb.common.utils.ShiroUtils;
import com.yubb.platform.service.saas.ISaaSMenuService;
import com.yubb.system.service.ISysMenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 *@Description 菜单信息
 *@Author zhushuyong
 *@Date 2021/5/26 14:55
 *@since:
 *@copyright: 版权所有2021 开源组织 gitee(https://gitee.com/jinzheyi)作者：朱述勇<br/>
 *            GitHub(https://github.com/jinzheyi)作者：朱述勇 。
 */
@Controller
@RequestMapping("/saas/menu")
public class SysMenuController extends BaseController
{

    @Autowired
    private ISysMenuService sysMenuService;

    @Autowired
    private ISaaSMenuService saaSMenuService;

    /**
     * 加载角色菜单列表树
     */
    @GetMapping("/roleMenuTreeData")
    @ResponseBody
    public List<Ztree> roleMenuTreeData(SysRoleDTO role)
    {
        List<Ztree> ztrees = sysMenuService.roleMenuTreeData(role, ShiroUtils.getUserId());
        return ztrees;
    }

    /**
     * 加载平台角色菜单列表树
     */
    @GetMapping("/pfRoleMenuTreeData")
    @ResponseBody
    public List<Ztree> pfRoleMenuTreeData(SysRoleDTO role)
    {
        List<Ztree> ztrees = saaSMenuService.roleMenuTreeData(role, false);
        return ztrees;
    }

}