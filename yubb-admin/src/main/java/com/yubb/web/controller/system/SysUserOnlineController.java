package com.yubb.web.controller.system;

import java.util.List;
import com.yubb.common.utils.bean.DozerUtils;
import com.yubb.system.domain.dto.SysUserOnlineDTO;
import com.yubb.system.domain.vo.SysUserOnlineVO;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.yubb.common.annotation.Log;
import com.yubb.common.core.controller.BaseController;
import com.yubb.common.core.domain.AjaxResult;
import com.yubb.common.core.page.TableDataInfo;
import com.yubb.common.core.text.Convert;
import com.yubb.common.enums.BusinessType;
import com.yubb.common.enums.OnlineStatus;
import com.yubb.common.utils.ShiroUtils;
import com.yubb.framework.shiro.session.OnlineSession;
import com.yubb.framework.shiro.session.OnlineSessionDAO;
import com.yubb.system.service.ISysUserOnlineService;

/**
 *@Description 在线用户监控
 *@Author zhushuyong
 *@Date 2021/5/26 14:42
 *@since:
 *@copyright: 版权所有2021 开源组织 gitee(https://gitee.com/jinzheyi)作者：朱述勇<br/>
 *            GitHub(https://github.com/jinzheyi)作者：朱述勇 。
 */
@Controller
@RequestMapping("/monitor/online")
public class SysUserOnlineController extends BaseController
{
    private String prefix = "monitor/online";

    @Autowired
    private ISysUserOnlineService userOnlineService;

    @Autowired
    private OnlineSessionDAO onlineSessionDAO;

    @RequiresPermissions("monitor:online:view")
    @GetMapping()
    public String online()
    {
        return prefix + "/online";
    }

    @RequiresPermissions("monitor:online:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo<SysUserOnlineVO> list(SysUserOnlineDTO userOnline)
    {
        return userOnlineService.selectUserOnlinePage(userOnline);
    }

    @RequiresPermissions(value = { "monitor:online:batchForceLogout", "monitor:online:forceLogout" }, logical = Logical.OR)
    @Log(title = "在线用户", businessType = BusinessType.FORCE)
    @PostMapping("/batchForceLogout")
    @ResponseBody
    public AjaxResult batchForceLogout(String ids)
    {
        for (String sessionId : Convert.toStrArray(ids))
        {
            SysUserOnlineVO online = userOnlineService.selectOnlineById(sessionId);
            if (online == null)
            {
                return error("用户已下线");
            }
            OnlineSession onlineSession = (OnlineSession) onlineSessionDAO.readSession(online.getId());
            if (onlineSession == null)
            {
                return error("用户已下线");
            }
            if (sessionId.equals(ShiroUtils.getSessionId()))
            {
                return error("当前登录用户无法强退");
            }
            onlineSessionDAO.delete(onlineSession);
            online.setStatus(OnlineStatus.off_line);
            online.setTenantId(ShiroUtils.getTenantId());
            userOnlineService.saveOnline(DozerUtils.copyProperties(online, SysUserOnlineDTO.class));
            userOnlineService.removeUserCache(online.getLoginName(), sessionId);
        }
        return success();
    }
}
