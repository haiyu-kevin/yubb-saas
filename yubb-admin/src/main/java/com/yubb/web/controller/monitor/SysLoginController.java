package com.yubb.web.controller.monitor;

import com.yubb.common.core.controller.BaseController;
import com.yubb.common.core.domain.AjaxResult;
import com.yubb.common.enums.LoginType;
import com.yubb.common.utils.ServletUtils;
import com.yubb.common.utils.StringUtils;
import com.yubb.framework.shiro.realm.token.CustomToken;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.subject.Subject;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *@Description 登录验证
 *@Author zhushuyong
 *@Date 2021/5/26 14:43
 *@since:
 *@copyright: 版权所有2021 开源组织 gitee(https://gitee.com/jinzheyi)作者：朱述勇<br/>
 *            GitHub(https://github.com/jinzheyi)作者：朱述勇 。
 */
@Controller
public class SysLoginController extends BaseController
{
    @GetMapping("/saas/login")
    public String login(HttpServletRequest request, HttpServletResponse response)
    {
        // 如果是Ajax请求，返回Json字符串。
        if (ServletUtils.isAjaxRequest(request))
        {
            return ServletUtils.renderString(response, "{\"code\":\"1\",\"msg\":\"未登录或登录超时。请重新登录\"}");
        }

        return "login";
    }

    @GetMapping("/platform/login")
    public String platFormLogin(HttpServletRequest request, HttpServletResponse response)
    {
        // 如果是Ajax请求，返回Json字符串。
        if (ServletUtils.isAjaxRequest(request))
        {
            return ServletUtils.renderString(response, "{\"code\":\"1\",\"msg\":\"未登录或登录超时。请重新登录\"}");
        }

        return "platform/login";
    }

    @PostMapping("/saas/login")
    @ResponseBody
    public AjaxResult ajaxLogin(String username, String password, String tenantNo, Boolean rememberMe)
    {
        CustomToken token = new CustomToken(username, password, rememberMe, tenantNo, LoginType.SAAS_USER.getType());
        Subject subject = SecurityUtils.getSubject();
        try
        {
            subject.login(token);
            return success();
        }
        catch (AuthenticationException e)
        {
            String msg = "用户或密码错误";
            if (StringUtils.isNotEmpty(e.getMessage()))
            {
                msg = e.getMessage();
            }
            return error(msg);
        }
    }

    @PostMapping("/platform/login")
    @ResponseBody
    public AjaxResult ajaxPlatFormLogin(String username, String password, Boolean rememberMe)
    {
        CustomToken token = new CustomToken(username, password, rememberMe, LoginType.PLATFORM_USER.getType());
        Subject subject = SecurityUtils.getSubject();
        try
        {
            subject.login(token);
            return success();
        }
        catch (AuthenticationException e)
        {
            String msg = "用户或密码错误";
            if (StringUtils.isNotEmpty(e.getMessage()))
            {
                msg = e.getMessage();
            }
            return error(msg);
        }
    }

    @GetMapping("/unauth")
    public String unauth()
    {
        return "error/unauth";
    }
}
