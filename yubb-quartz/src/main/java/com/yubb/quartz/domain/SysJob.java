package com.yubb.quartz.domain;

import com.baomidou.mybatisplus.annotation.TableName;
import com.yubb.common.constant.ScheduleConstants;
import com.yubb.common.core.domain.platform.base.PlatformDO;
import com.yubb.common.utils.StringUtils;
import com.yubb.quartz.util.CronUtils;
import lombok.*;
import lombok.experimental.SuperBuilder;

import java.util.Date;

/**
 *@Description 定时任务调度表
 *@Author zhushuyong
 *@Date 2021/6/23 22:54
 *@since:
 *@copyright: 版权所有2021 开源组织 gitee(https://gitee.com/jinzheyi)作者：朱述勇<br/>
 *            GitHub(https://github.com/jinzheyi)作者：朱述勇 。
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder(toBuilder = true)
@EqualsAndHashCode(callSuper = true)
@TableName("sys_job")
public class SysJob extends PlatformDO
{
    private static final long serialVersionUID = 1L;

    /** 任务名称 */
    private String jobName;

    /** 任务组名 */
    private String jobGroup;

    /** 调用目标字符串 */
    private String invokeTarget;

    /** cron执行表达式 */
    private String cronExpression;

    /** cron计划策略 */
    @Builder.Default
    private String misfirePolicy = ScheduleConstants.MISFIRE_DEFAULT;

    /** 是否并发执行（0允许 1禁止） */
    private String concurrent;

    /** 任务状态（0正常 1暂停） */
    private String status;

    /** 备注 */
    private String remark;


    public Date getNextValidTime()
    {
        if (StringUtils.isNotEmpty(cronExpression))
        {
            return CronUtils.getNextExecution(cronExpression);
        }
        return null;
    }

}