package com.yubb.quartz.service.impl;

import java.util.Arrays;
import java.util.List;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yubb.common.utils.bean.DozerUtils;
import com.yubb.quartz.domain.dto.SysJobLogDTO;
import com.yubb.quartz.domain.vo.SysJobLogVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.yubb.common.core.text.Convert;
import com.yubb.quartz.domain.SysJobLog;
import com.yubb.quartz.mapper.SysJobLogMapper;
import com.yubb.quartz.service.ISysJobLogService;

/**
 *@Description 定时任务调度日志信息 服务层
 *@Author zhushuyong
 *@Date 2021/6/23 23:26
 *@since:
 *@copyright: 版权所有2021 开源组织 gitee(https://gitee.com/jinzheyi)作者：朱述勇<br/>
 *            GitHub(https://github.com/jinzheyi)作者：朱述勇 。
 */
@Service
public class SysJobLogServiceImpl extends ServiceImpl<SysJobLogMapper, SysJobLog> implements ISysJobLogService
{
    @Autowired
    private SysJobLogMapper jobLogMapper;

    /**
     * 获取quartz调度器日志的计划任务
     * 
     * @param jobLog 调度日志信息
     * @return 调度任务日志集合
     */
    @Override
    public List<SysJobLogVO> selectJobLogList(SysJobLogDTO jobLog)
    {
        return jobLogMapper.selectJobLogList(jobLog);
    }

    /**
     * 通过调度任务日志ID查询调度信息
     * 
     * @param jobLogId 调度任务日志ID
     * @return 调度任务日志对象信息
     */
    @Override
    public SysJobLogVO selectJobLogById(String jobLogId)
    {
        return DozerUtils.copyProperties(jobLogMapper.selectById(jobLogId), SysJobLogVO.class);
    }

    /**
     * 新增任务日志
     * 
     * @param jobLog 调度日志信息
     */
    @Override
    public void addJobLog(SysJobLogDTO jobLog)
    {
        jobLogMapper.insert(DozerUtils.copyProperties(jobLog, SysJobLog.class));
    }

    /**
     * 批量删除调度日志信息
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteJobLogByIds(String ids)
    {
        return jobLogMapper.deleteBatchIds(Arrays.asList(Convert.toStrArray(ids)));
    }

    /**
     * 删除任务日志
     * 
     * @param jobId 调度日志ID
     */
    @Override
    public int deleteJobLogById(String jobId)
    {
        return jobLogMapper.deleteById(jobId);
    }

    /**
     * 清空任务日志
     */
    @Override
    public void cleanJobLog()
    {
        jobLogMapper.cleanJobLog();
    }
}
