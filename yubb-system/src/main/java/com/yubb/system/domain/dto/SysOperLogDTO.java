package com.yubb.system.domain.dto;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.yubb.common.core.domain.saas.base.BaseDTO;
import lombok.*;
import lombok.experimental.SuperBuilder;

import java.util.Date;

/**
 *@Description 操作日志记录DTO
 *@Author zhushuyong
 *@Date 2021/6/19 21:47
 *@since:
 *@copyright: 版权所有2021 开源组织 gitee(https://gitee.com/jinzheyi)作者：朱述勇<br/>
 *            GitHub(https://github.com/jinzheyi)作者：朱述勇 。
 */
@Data
@SuperBuilder(toBuilder = true)
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class SysOperLogDTO extends BaseDTO {
    private static final long serialVersionUID = 1L;

    private String id;

    /** 操作模块 */
    private String title;

    /** 业务类型（0其它 1新增 2修改 3删除） */
    private Integer businessType;

    /** 业务类型数组 */
    private Integer[] businessTypes;

    /** 请求方法 */
    private String method;

    /** 请求方式 */
    private String requestMethod;

    /** 操作类别（0其它 1后台用户 2手机端用户） */
    private Integer operatorType;

    /** 操作人员 */
    private String operName;

    /** 部门名称 */
    private String deptName;

    /** 请求url */
    private String operUrl;

    /** 操作地址 */
    private String operIp;

    /** 操作地点 */
    private String operLocation;

    /** 请求参数 */
    private String operParam;

    /** 返回参数 */
    private String jsonResult;

    /** 操作状态（0正常 1异常） */
    private Integer status;

    private String tenantId;

    /** 错误消息 */
    private String errorMsg;

    /** 操作时间 */
    @TableField(fill = FieldFill.INSERT)
    @Builder.Default
    private Date operTime = new Date();

}
