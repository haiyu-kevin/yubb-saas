package com.yubb.system.service.impl;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yubb.common.core.page.MpPageUtils;
import com.yubb.common.core.page.TableDataInfo;
import com.yubb.common.utils.bean.DozerUtils;
import com.yubb.system.domain.SysUserPost;
import com.yubb.system.domain.dto.SysPostDTO;
import com.yubb.system.domain.vo.SysPostVO;
import org.springframework.stereotype.Service;
import com.yubb.common.constant.UserConstants;
import com.yubb.common.core.text.Convert;
import com.yubb.common.exception.BusinessException;
import com.yubb.common.utils.StringUtils;
import com.yubb.system.domain.SysPost;
import com.yubb.system.mapper.SysPostMapper;
import com.yubb.system.mapper.SysUserPostMapper;
import com.yubb.system.service.ISysPostService;
import javax.annotation.Resource;

/**
 *@Description 岗位信息 服务层处理
 *@Author zhushuyong
 *@Date 2021/5/27 12:20
 *@since:
 *@copyright: 版权所有2021 开源组织 gitee(https://gitee.com/jinzheyi)作者：朱述勇<br/>
 *            GitHub(https://github.com/jinzheyi)作者：朱述勇 。
 */
@Service
public class SysPostServiceImpl extends ServiceImpl<SysPostMapper, SysPost> implements ISysPostService {

    @Resource
    private SysPostMapper postMapper;

    @Resource
    private SysUserPostMapper userPostMapper;

    /**
     * 查询岗位信息集合
     * 
     * @param postDTO 岗位信息
     * @return 岗位信息集合
     */
    @Override
    public TableDataInfo<SysPostVO> selectPostPage(SysPostDTO postDTO) {
        return MpPageUtils.copyPage(this.page(MpPageUtils.intPage(), getWrapper(postDTO)), SysPostVO.class);
    }

    @Override
    public List<SysPostVO> selectPostList(SysPostDTO postDTO) {
        return BeanUtil.copyToList(this.list(getWrapper(postDTO)), SysPostVO.class);
    }

    private LambdaQueryWrapper<SysPost> getWrapper(SysPostDTO postDTO) {
        return new LambdaQueryWrapper<SysPost>().eq(StringUtils.isNotBlank(postDTO.getStatus()),SysPost::getStatus,postDTO.getStatus())
                .like(StringUtils.isNotBlank(postDTO.getPostCode()),SysPost::getPostCode,postDTO.getPostCode())
                .like(StringUtils.isNotBlank(postDTO.getPostName()),SysPost::getPostName,postDTO.getPostName());
    }

    /**
     * 查询所有岗位
     * 
     * @return 岗位列表
     */
    @Override
    public List<SysPostVO> selectPostAll() {
        return BeanUtil.copyToList(list(), SysPostVO.class);
    }

    /**
     * 根据用户ID查询岗位
     * 
     * @param userId 用户ID
     * @return 岗位列表
     */
    @Override
    public List<SysPostVO> selectPostsByUserId(String userId) {
        List<SysPostVO> userPosts = postMapper.selectPostsByUserId(userId);
        List<SysPostVO> posts = selectPostAll();
        for (SysPostVO postVO : posts) {
            for (SysPostVO userRole : userPosts) {
                if (postVO.getId().equals(userRole.getId())) {
                    postVO.setFlag(true);
                    break;
                }
            }
        }
        return posts;
    }

    /**
     * 通过岗位ID查询岗位信息
     * 
     * @param postId 岗位ID
     * @return 角色对象信息
     */
    @Override
    public SysPostVO selectPostById(String postId) {
        return DozerUtils.copyProperties(getById(postId),SysPostVO.class);
    }

    /**
     * 批量删除岗位信息
     * 
     * @param ids 需要删除的数据ID
     * @throws Exception
     */
    @Override
    public int deletePostByIds(String ids) throws BusinessException {
        String[] postIds = Convert.toStrArray(ids);
        for (String postId : postIds) {
            SysPostVO post = selectPostById(postId);
            if (countUserPostById(postId) > 0) {
                throw new BusinessException(String.format("%1$s已分配,不能删除", post.getPostName()));
            }
        }
        return postMapper.deleteBatchIds(Arrays.asList(postIds));
    }

    /**
     * 新增保存岗位信息
     * 
     * @param postDTO 岗位信息
     * @return 结果
     */
    @Override
    public int insertPost(SysPostDTO postDTO) {
        return postMapper.insert(DozerUtils.copyProperties(postDTO, SysPost.class));
    }

    /**
     * 修改保存岗位信息
     * 
     * @param postDTO 岗位信息
     * @return 结果
     */
    @Override
    public int updatePost(SysPostDTO postDTO) {
        return postMapper.updateById(DozerUtils.copyProperties(postDTO, SysPost.class));
    }

    /**
     * 通过岗位ID查询岗位使用数量
     * 
     * @param postId 岗位ID
     * @return 结果
     */
    @Override
    public long countUserPostById(String postId) {
        return userPostMapper.selectCount(Wrappers.query(SysUserPost.builder().postId(postId).build()));
    }

    /**
     * 校验岗位名称是否唯一
     * 
     * @param postDTO 岗位信息
     * @return 结果
     */
    @Override
    public String checkPostNameUnique(SysPostDTO postDTO)
    {
        String postId = StringUtils.isIdNull(postDTO.getId());
        Optional<SysPost> info = list(new LambdaQueryWrapper<SysPost>()
                .eq(StringUtils.isNotBlank(postDTO.getPostName())
                        ,SysPost::getPostName,
                        postDTO.getPostName())).stream().findFirst();
        if (info.isPresent() && !postId.equals(info.get().getId())) {
            return UserConstants.POST_NAME_NOT_UNIQUE;
        }
        return UserConstants.POST_NAME_UNIQUE;
    }

    /**
     * 校验岗位编码是否唯一
     * 
     * @param postDTO 岗位信息
     * @return 结果
     */
    @Override
    public String checkPostCodeUnique(SysPostDTO postDTO) {
        String postId = StringUtils.isIdNull(postDTO.getId());
        Optional<SysPost> info = list(new LambdaQueryWrapper<SysPost>()
                .eq(StringUtils.isNotBlank(postDTO.getPostCode())
                        ,SysPost::getPostCode,
                        postDTO.getPostCode())).stream().findFirst();
        if (info.isPresent() && !postId.equals(info.get().getId())) {
            return UserConstants.POST_CODE_NOT_UNIQUE;
        }
        return UserConstants.POST_CODE_UNIQUE;
    }

}
