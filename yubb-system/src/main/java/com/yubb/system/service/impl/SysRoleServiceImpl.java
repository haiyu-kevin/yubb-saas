package com.yubb.system.service.impl;

import cn.hutool.core.util.IdUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yubb.common.annotation.DataScope;
import com.yubb.common.constant.Constants;
import com.yubb.common.core.domain.saas.dto.SysRoleDTO;
import com.yubb.common.core.domain.saas.entity.SysRole;
import com.yubb.common.core.domain.saas.vo.SysRoleVO;
import com.yubb.common.core.domain.saas.vo.SysUserVO;
import com.yubb.common.core.text.Convert;
import com.yubb.common.exception.BusinessException;
import com.yubb.common.utils.StringUtils;
import com.yubb.common.utils.bean.DozerUtils;
import com.yubb.platform.domain.SysRoleMenu;
import com.yubb.platform.mapper.saas.SaaSRoleMapper;
import com.yubb.platform.service.ISysRoleMenuService;
import com.yubb.system.domain.SysRoleDept;
import com.yubb.system.domain.SysUserRole;
import com.yubb.system.mapper.SysRoleMapper;
import com.yubb.system.mapper.SysUserRoleMapper;
import com.yubb.system.service.ISysRoleDeptService;
import com.yubb.system.service.ISysRoleService;
import com.yubb.system.service.ISysUserRoleService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.*;

/**
 *@Description 角色 业务层处理
 *@Author zhushuyong
 *@Date 2021/6/6 16:46
 *@since:
 *@copyright: 版权所有2021 开源组织 gitee(https://gitee.com/jinzheyi)作者：朱述勇<br/>
 *            GitHub(https://github.com/jinzheyi)作者：朱述勇 。
 */
@Service
public class SysRoleServiceImpl extends ServiceImpl<SysRoleMapper, SysRole> implements ISysRoleService {

    @Resource
    private SysRoleMapper roleMapper;

    @Resource
    private SaaSRoleMapper saaSRoleMapper;

    @Resource
    private ISysRoleMenuService roleMenuService;

    @Resource
    private ISysRoleDeptService roleDeptService;

    @Resource
    private ISysUserRoleService userRoleService;

    /**
     * 根据条件分页查询角色数据
     * 
     * @param roleDTO 角色信息
     * @return 角色数据集合信息
     */
    @Override
    @DataScope(deptAlias = "d")
    public List<SysRoleVO> selectRoleList(SysRoleDTO roleDTO) {
        return roleMapper.selectRoleList(roleDTO);
    }

    @Override
    public List<SysRoleVO> selectRolesByUser(SysUserVO user){
        return saaSRoleMapper.selectRolesByUserId(user.getId());
    }

    /**
     * 根据用户ID查询权限
     * 
     * @param user 用户
     * @return 权限列表
     */
    @Override
    public Set<String> selectRoleKeys(SysUserVO user) {
        List<SysRoleVO> perms = selectRolesByUser(user);
        Set<String> permsSet = new HashSet<>();
        for (SysRoleVO perm : perms) {
            if (StringUtils.isNotNull(perm)) {
                permsSet.addAll(Arrays.asList(perm.getRoleKey().trim().split(",")));
            }
        }
        return permsSet;
    }

    /**
     * 根据用户ID查询角色
     * 
     * @param user 用户
     * @return 角色列表
     */
    @Override
    public List<SysRoleVO> selectRolesByUserId(SysUserVO user) {
        List<SysRoleVO> userRoles = selectRolesByUser(user);
        List<SysRoleVO> roles = selectRoleAll();
        for (SysRoleVO role : roles) {
            for (SysRoleVO userRole : userRoles) {
                if (role.getId().equals(userRole.getId())) {
                    role.setFlag(true);
                    break;
                }
            }
        }
        return roles;
    }

    /**
     * 查询所有角色
     * 
     * @return 角色列表
     */
    @Override
    public List<SysRoleVO> selectRoleAll() {
        List<SysRoleVO> roleVOList = saaSRoleMapper.selectRoleList(new SysRoleDTO());
        roleVOList.addAll(this.selectRoleList(new SysRoleDTO()));
        return roleVOList;
    }

    /**
     * 通过角色ID查询角色
     * 
     * @param roleId 角色ID
     * @return 角色对象信息
     */
    @Override
    public SysRoleVO selectRoleById(String roleId) {
        return DozerUtils.copyProperties(roleMapper.selectOne(Wrappers.query(SysRole.builder()
                .id(roleId)
                .delFlag(Constants.DEL_FLAG_NORMAL)
                .build())),SysRoleVO.class);
    }

    /**
     * 通过角色ID删除角色
     * 
     * @param roleId 角色ID
     * @return 结果
     */
    @Override
    @Transactional
    public boolean deleteRoleById(String roleId) {
        // 删除角色与菜单关联
        roleMenuService.remove(Wrappers.query(SysRoleMenu.builder().roleId(roleId).build()));
        // 删除角色与部门关联
        roleDeptService.remove(Wrappers.query(SysRoleDept.builder().roleId(roleId).build()));
        return roleMapper.update(SysRole.builder().delFlag(Constants.DEL_FLAG_DEL).build(),
                Wrappers.query(SysRole.builder().id(roleId).build())) > 0 ? true : false;
    }

    /**
     * 批量删除角色信息
     * 
     * @param ids 需要删除的数据ID
     * @throws Exception
     */
    @Override
    @Transactional
    public int deleteRoleByIds(String ids) {
        String[] roleIds = Convert.toStrArray(ids);
        for (String roleId : roleIds) {
            checkRoleAllowed(SysRoleDTO.builder().id(roleId).build());
            SysRoleVO role = selectRoleById(roleId);
            if (countUserRoleByRoleId(roleId) > 0) {
                throw new BusinessException(String.format("%1$s已分配,不能删除", role.getRoleName()));
            }
        }
        // 删除角色与菜单关联
        roleMenuService.remove(new LambdaQueryWrapper<SysRoleMenu>().in(SysRoleMenu::getRoleId, Arrays.asList(roleIds)));
        // 删除角色与部门关联
        roleDeptService.remove(new LambdaQueryWrapper<SysRoleDept>().in(SysRoleDept::getRoleId, Arrays.asList(roleIds)));
        return roleMapper.deleteBatchIds(Arrays.asList(roleIds));
    }

    /**
     * 新增保存角色信息
     * 
     * @param roleDTO 角色信息
     * @return 结果
     */
    @Override
    @Transactional
    public boolean insertRole(SysRoleDTO roleDTO) {
        // 新增角色信息
        SysRole sysRole = DozerUtils.copyProperties(roleDTO, SysRole.class);
        sysRole.setRoleKey(IdUtil.fastSimpleUUID());
        roleMapper.insert(sysRole);
        roleDTO.setId(sysRole.getId());
        return insertRoleMenu(roleDTO);
    }

    /**
     * 修改保存角色信息
     * 
     * @param roleDTO 角色信息
     * @return 结果
     */
    @Override
    @Transactional
    public boolean updateRole(SysRoleDTO roleDTO) {
        // 修改角色信息
        roleMapper.updateById(DozerUtils.copyProperties(roleDTO, SysRole.class));
        // 删除角色与菜单关联
        roleMenuService.remove(Wrappers.query(SysRoleMenu.builder().roleId(roleDTO.getId()).build()));
        return insertRoleMenu(roleDTO);
    }

    /**
     * 修改数据权限信息
     * 
     * @param roleDTO 角色信息
     * @return 结果
     */
    @Override
    @Transactional
    public boolean authDataScope(SysRoleDTO roleDTO) {
        // 修改角色信息
        roleMapper.updateById(DozerUtils.copyProperties(roleDTO, SysRole.class));
        // 删除角色与部门关联
        roleDeptService.remove(Wrappers.query(SysRoleDept.builder().roleId(roleDTO.getId()).build()));
        // 新增角色和部门信息（数据权限）
        return insertRoleDept(roleDTO);
    }

    /**
     * 新增角色菜单信息
     * 
     * @param role 角色对象
     */
    public boolean insertRoleMenu(SysRoleDTO role) {
        boolean rows = true;
        // 新增用户与角色管理
        List<SysRoleMenu> list = new ArrayList<SysRoleMenu>();
        for (String menuId : role.getMenuIds()) {
            list.add(SysRoleMenu.builder().roleId(role.getId()).menuId(menuId).build());
        }
        if (list.size() > 0) {
            rows = roleMenuService.saveBatch(list);
        }
        return rows;
    }

    /**
     * 新增角色部门信息(数据权限)
     *
     * @param roleDTO 角色对象
     */
    public boolean insertRoleDept(SysRoleDTO roleDTO) {
        boolean rows = true;
        // 新增角色与部门（数据权限）管理
        List<SysRoleDept> list = new ArrayList<SysRoleDept>();
        for (String deptId : roleDTO.getDeptIds()) {
            list.add(SysRoleDept.builder().roleId(roleDTO.getId()).deptId(deptId).build());
        }
        if (list.size() > 0) {
            rows = roleDeptService.saveBatch(list);
        }
        return rows;
    }

    /**
     * 校验角色是否允许操作
     * 
     * @param roleDTO 角色信息
     */
    @Override
    public void checkRoleAllowed(SysRoleDTO roleDTO) {
        if (StringUtils.isNotBlank(roleDTO.getId()) && this.getById(roleDTO.getId()).isAdmin()) {
            throw new BusinessException("不允许操作超级管理员角色");
        }
    }

    /**
     * 通过角色ID查询角色使用数量
     * 
     * @param roleId 角色ID
     * @return 结果
     */
    @Override
    public long countUserRoleByRoleId(String roleId) {
        return userRoleService.count(Wrappers.query(SysUserRole.builder().roleId(roleId).build()));
    }

    /**
     * 角色状态修改
     * 
     * @param roleDTO 角色信息
     * @return 结果
     */
    @Override
    public int changeStatus(SysRoleDTO roleDTO) {
        return roleMapper.updateById(DozerUtils.copyProperties(roleDTO,SysRole.class));
    }

    /**
     * 取消授权用户角色
     * 
     * @param userRole 用户和角色关联信息
     * @return 结果
     */
    @Override
    public boolean deleteAuthUser(SysUserRole userRole) {
        return userRoleService.remove(Wrappers.query(SysUserRole.builder()
                .roleId(userRole.getRoleId())
                .userId(userRole.getUserId())
                .build()));
    }

    /**
     * 批量取消授权用户角色
     * 
     * @param roleId 角色ID
     * @param userIds 需要删除的用户数据ID
     * @return 结果
     */
    @Override
    public boolean deleteAuthUsers(String roleId, String userIds) {
        return userRoleService.remove(new LambdaQueryWrapper<SysUserRole>()
                .eq(SysUserRole::getRoleId, roleId)
                .in(SysUserRole::getUserId, Arrays.asList(Convert.toStrArray(userIds))));
    }

    /**
     * 批量选择授权用户角色
     * 
     * @param roleId 角色ID
     * @param userIds 需要删除的用户数据ID
     * @return 结果
     */
    @Override
    public boolean insertAuthUsers(String roleId, String userIds) {
        String[] users = Convert.toStrArray(userIds);
        boolean row = true;
        // 新增用户与角色管理
        List<SysUserRole> list = new ArrayList<SysUserRole>();
        for (String userId : users) {
            list.add(SysUserRole.builder().userId(userId).roleId(roleId).build());
        }
        if (list.size()>0) {
            row = userRoleService.saveBatch(list);
        }
        return row;
    }
}
