package com.yubb.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yubb.system.domain.SysRoleDept;
import com.yubb.system.mapper.SysRoleDeptMapper;
import com.yubb.system.service.ISysRoleDeptService;
import org.springframework.stereotype.Service;

/**
 * @Description 角色部门接口实现
 * @Author zhushuyong
 * @Date 2021/6/19 16:10
 * @since:
 * @copyright: 版权所有2021 开源组织 gitee(https://gitee.com/jinzheyi)作者：朱述勇<br/>
 * GitHub(https://github.com/jinzheyi)作者：朱述勇 。
 */
@Service
public class SysRoleDeptServiceImpl extends ServiceImpl<SysRoleDeptMapper, SysRoleDept> implements ISysRoleDeptService {
}
