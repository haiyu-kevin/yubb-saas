package com.yubb.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yubb.common.core.domain.Ztree;
import com.yubb.common.core.domain.platform.SysMenu;
import com.yubb.common.core.domain.platform.vo.SysMenuVO;
import com.yubb.common.core.domain.saas.dto.SysRoleDTO;
import com.yubb.common.core.domain.saas.dto.SysUserDTO;
import com.yubb.common.core.domain.saas.vo.SysUserVO;

import java.util.List;
import java.util.Set;

/**
 * @Description TODO
 * @Author zhushuyong
 * @Date 2021/8/3 16:18
 * @since:
 * @copyright: 版权所有2021 开源组织 gitee(https://gitee.com/jinzheyi)作者：朱述勇<br/>
 * GitHub(https://github.com/jinzheyi)作者：朱述勇 。
 */
public interface ISysMenuService extends IService<SysMenu> {

    /**
     * 根据用户ID查询权限
     *
     * @param userVO 用户ID
     * @return 权限列表
     */
    public Set<String> selectPermsByUserId(SysUserVO userVO);

    /**
     * 根据用户ID查询菜单
     *
     * @param user 用户信息
     * @return 菜单列表
     */
    public List<SysMenuVO> selectMenusByUser(SysUserDTO user);

    /**
     * 根据角色ID查询菜单
     *
     * @param role 角色对象
     * @param userId 用户ID
     * @return 菜单列表
     */
    public List<Ztree> roleMenuTreeData(SysRoleDTO role, String userId);

}
