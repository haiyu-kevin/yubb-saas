package com.yubb.system.service;

import java.util.List;
import com.baomidou.mybatisplus.extension.service.IService;
import com.yubb.common.core.page.TableDataInfo;
import com.yubb.system.domain.SysNotice;
import com.yubb.system.domain.dto.SysNoticeDTO;
import com.yubb.system.domain.vo.SysNoticeVO;

/**
 *@Description 公告 服务层
 *@Author zhushuyong
 *@Date 2021/6/21 22:39
 *@since:
 *@copyright: 版权所有2021 开源组织 gitee(https://gitee.com/jinzheyi)作者：朱述勇<br/>
 *            GitHub(https://github.com/jinzheyi)作者：朱述勇 。
 */
public interface ISysNoticeService extends IService<SysNotice> {
    /**
     * 查询公告信息
     * 
     * @param noticeId 公告ID
     * @return 公告信息
     */
    public SysNoticeVO selectNoticeById(String noticeId);

    /**
     * 查询公告列表
     * 
     * @param notice 公告信息
     * @return 公告集合
     */
    public TableDataInfo<SysNoticeVO> selectNoticeList(SysNoticeDTO notice);

    /**
     * 新增公告
     * 
     * @param notice 公告信息
     * @return 结果
     */
    public int insertNotice(SysNoticeDTO notice);

    /**
     * 修改公告
     * 
     * @param notice 公告信息
     * @return 结果
     */
    public int updateNotice(SysNoticeDTO notice);

    /**
     * 删除公告信息
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteNoticeByIds(String ids);
}
