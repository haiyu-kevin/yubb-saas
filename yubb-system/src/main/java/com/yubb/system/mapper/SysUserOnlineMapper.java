package com.yubb.system.mapper;

import com.baomidou.mybatisplus.annotation.InterceptorIgnore;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yubb.system.domain.SysUserOnline;
import com.yubb.system.domain.dto.SysUserOnlineDTO;
import com.yubb.system.domain.vo.SysUserOnlineVO;
import org.apache.ibatis.annotations.Mapper;
import java.util.List;

/**
 *@Description 在线用户 数据层
 *@Author zhushuyong
 *@Date 2021/6/19 22:37
 *@since:
 *@copyright: 版权所有2021 开源组织 gitee(https://gitee.com/jinzheyi)作者：朱述勇<br/>
 *            GitHub(https://github.com/jinzheyi)作者：朱述勇 。
 */
@Mapper
public interface SysUserOnlineMapper extends BaseMapper<SysUserOnline> {

//    /**
//     * 查询过期会话集合
//     *
//     * @param lastAccessTime 过期时间
//     * @return 会话集合
//     */
//    @InterceptorIgnore(tenantLine = "true")
//    public List<SysUserOnlineVO> selectOnlineByExpired(String lastAccessTime);

//    /**
//     * 保存会话信息
//     *
//     * @param online 会话信息
//     * @return 结果
//     */
//    @InterceptorIgnore(tenantLine = "true")
//    public int saveOnline(SysUserOnlineDTO online);

//    /**
//     * 通过会话序号查询信息
//     *
//     * @param sessionId 会话ID
//     * @return 在线用户信息
//     */
//    @InterceptorIgnore(tenantLine = "true")
//    public SysUserOnlineVO selectOnlineById(String sessionId);

//    /**
//     * 通过会话序号删除信息
//     *
//     * @param sessionId 会话ID
//     * @return 在线用户信息
//     */
//    @InterceptorIgnore(tenantLine = "true")
//    public int deleteOnlineById(String sessionId);

}
