package com.yubb.platform.service;

import java.util.List;
import com.baomidou.mybatisplus.extension.service.IService;
import com.yubb.common.core.domain.platform.SysConfig;
import com.yubb.common.core.domain.platform.dto.SysConfigDTO;
import com.yubb.common.core.domain.platform.vo.SysConfigVO;

/**
 *@Description 参数配置 服务层
 *@Author zhushuyong
 *@Date 2021/6/21 14:16
 *@since:
 *@copyright: 版权所有2021 开源组织 gitee(https://gitee.com/jinzheyi)作者：朱述勇<br/>
 *            GitHub(https://github.com/jinzheyi)作者：朱述勇 。
 */
public interface ISysConfigService extends IService<SysConfig> {
    /**
     * 查询参数配置信息
     * 
     * @param configId 参数配置ID
     * @return 参数配置信息
     */
    public SysConfigVO selectConfigById(String configId);

    /**
     * 根据键名查询参数配置信息
     * 
     * @param configKey 参数键名
     * @return 参数键值
     */
    public String selectConfigByKey(String configKey);

    /**
     * 查询参数配置列表
     * 
     * @param config 参数配置信息
     * @return 参数配置集合
     */
    public List<SysConfigVO> selectConfigList(SysConfigDTO config);

    /**
     * 新增参数配置
     * 
     * @param config 参数配置信息
     * @return 结果
     */
    public int insertConfig(SysConfigDTO config);

    /**
     * 修改参数配置
     * 
     * @param config 参数配置信息
     * @return 结果
     */
    public int updateConfig(SysConfigDTO config);

    /**
     * 批量删除参数配置信息
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteConfigByIds(String ids);

    /**
     * 清空缓存数据
     */
    public void clearCache();

    /**
     * 校验参数键名是否唯一
     * 
     * @param config 参数信息
     * @return 结果
     */
    public String checkConfigKeyUnique(SysConfigDTO config);

}
