package com.yubb.platform.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yubb.platform.domain.PlatformRoleDept;

/**
 * @Description 角色部门接口
 * @Author zhushuyong
 * @Date 2021/6/19 16:08
 * @since:
 * @copyright: 版权所有2021 开源组织 gitee(https://gitee.com/jinzheyi)作者：朱述勇<br/>
 * GitHub(https://github.com/jinzheyi)作者：朱述勇 。
 */
public interface IPlatformRoleDeptService extends IService<PlatformRoleDept> {
}
