package com.yubb.platform.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yubb.platform.domain.PlatformRoleMenu;
import com.yubb.platform.mapper.PlatformRoleMenuMapper;
import com.yubb.platform.service.IPlatformRoleMenuService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * @Description 平台角色菜单接口实现
 * @Author zhushuyong
 * @Date 2021/7/15 22:26
 * @since:
 * @copyright: 版权所有2021 开源组织 gitee(https://gitee.com/jinzheyi)作者：朱述勇<br/>
 * GitHub(https://github.com/jinzheyi)作者：朱述勇 。
 */
@Slf4j
@Service
public class PlatformRoleMenuServiceImpl extends ServiceImpl<PlatformRoleMenuMapper, PlatformRoleMenu>
        implements IPlatformRoleMenuService {
}
