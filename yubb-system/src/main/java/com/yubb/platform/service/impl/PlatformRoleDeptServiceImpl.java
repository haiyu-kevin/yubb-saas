package com.yubb.platform.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yubb.platform.domain.PlatformRoleDept;
import com.yubb.platform.mapper.PlatformRoleDeptMapper;
import com.yubb.platform.service.IPlatformRoleDeptService;
import org.springframework.stereotype.Service;

/**
 * @Description 角色部门接口实现
 * @Author zhushuyong
 * @Date 2021/6/19 16:10
 * @since:
 * @copyright: 版权所有2021 开源组织 gitee(https://gitee.com/jinzheyi)作者：朱述勇<br/>
 * GitHub(https://github.com/jinzheyi)作者：朱述勇 。
 */
@Service
public class PlatformRoleDeptServiceImpl extends ServiceImpl<PlatformRoleDeptMapper, PlatformRoleDept>
        implements IPlatformRoleDeptService {
}
