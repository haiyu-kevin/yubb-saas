package com.yubb.platform.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yubb.platform.domain.PlatformUserPost;
import com.yubb.platform.mapper.PlatformUserPostMapper;
import com.yubb.platform.service.IPlatformUserPostService;
import org.springframework.stereotype.Service;

/**
 * @Description 业务接口实现
 * @Author zhushuyong
 * @Date 2021/6/17 21:27
 * @since:
 * @copyright: 版权所有2021 开源组织 gitee(https://gitee.com/jinzheyi)作者：朱述勇<br/>
 * GitHub(https://github.com/jinzheyi)作者：朱述勇 。
 */
@Service
public class PlatformUserPostServiceImpl extends ServiceImpl<PlatformUserPostMapper, PlatformUserPost> implements IPlatformUserPostService {
}
