package com.yubb.platform.service.impl;

import java.util.Arrays;
import java.util.List;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yubb.common.core.domain.platform.dto.SysDictDataDTO;
import com.yubb.common.core.domain.platform.vo.SysDictDataVO;
import com.yubb.common.utils.bean.DozerUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.yubb.common.core.domain.platform.SysDictData;
import com.yubb.common.core.text.Convert;
import com.yubb.common.utils.DictUtils;
import com.yubb.platform.mapper.SysDictDataMapper;
import com.yubb.platform.service.ISysDictDataService;

/**
 *@Description 字典 业务层处理
 *@Author zhushuyong
 *@Date 2021/6/17 14:21
 *@since:
 *@copyright: 版权所有2021 开源组织 gitee(https://gitee.com/jinzheyi)作者：朱述勇<br/>
 *            GitHub(https://github.com/jinzheyi)作者：朱述勇 。
 */
@Service
public class SysDictDataServiceImpl extends ServiceImpl<SysDictDataMapper, SysDictData>
        implements ISysDictDataService {
    @Autowired
    private SysDictDataMapper dictDataMapper;

    /**
     * 根据条件分页查询字典数据
     * 
     * @param dictData 字典数据信息
     * @return 字典数据集合信息
     */
    @Override
    public List<SysDictDataVO> selectDictDataList(SysDictDataDTO dictData)
    {
        return dictDataMapper.selectDictDataList(dictData);
    }

    /**
     * 根据字典类型和字典键值查询字典数据信息
     * 
     * @param dictType 字典类型
     * @param dictValue 字典键值
     * @return 字典标签
     */
    @Override
    public String selectDictLabel(String dictType, String dictValue)
    {
        return dictDataMapper.selectDictLabel(dictType, dictValue);
    }

    /**
     * 根据字典数据ID查询信息
     * 
     * @param dictCode 字典数据ID
     * @return 字典数据
     */
    @Override
    public SysDictDataVO selectDictDataById(String dictCode)
    {
        return DozerUtils.copyProperties(dictDataMapper.selectById(dictCode), SysDictDataVO.class);
    }

    /**
     * 批量删除字典数据
     * 
     * @param ids 需要删除的数据
     * @return 结果
     */
    @Override
    public int deleteDictDataByIds(String ids)
    {
        int row = dictDataMapper.deleteBatchIds(Arrays.asList(Convert.toStrArray(ids)));
        if (row > 0) {
            DictUtils.clearDictCache();
        }
        return row;
    }

    /**
     * 新增保存字典数据信息
     * 
     * @param dictData 字典数据信息
     * @return 结果
     */
    @Override
    public int insertDictData(SysDictDataDTO dictData)
    {
        int row = dictDataMapper.insert(DozerUtils.copyProperties(dictData, SysDictData.class));
        if (row > 0)
        {
            DictUtils.clearDictCache();
        }
        return row;
    }

    /**
     * 修改保存字典数据信息
     * 
     * @param dictData 字典数据信息
     * @return 结果
     */
    @Override
    public int updateDictData(SysDictDataDTO dictData)
    {
        int row = dictDataMapper.updateById(DozerUtils.copyProperties(dictData, SysDictData.class));
        if (row > 0)
        {
            DictUtils.clearDictCache();
        }
        return row;
    }
}
