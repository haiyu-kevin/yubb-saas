package com.yubb.platform.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yubb.common.core.text.Convert;
import com.yubb.platform.domain.PlatformLogininfor;
import com.yubb.platform.domain.dto.PlatformLogininforDTO;
import com.yubb.platform.domain.vo.PlatformLogininforVO;
import com.yubb.platform.mapper.PlatformLogininforMapper;
import com.yubb.platform.service.IPlatformLogininforService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

/**
 *@Description 系统访问日志情况信息 服务层处理
 *@Author zhushuyong
 *@Date 2021/6/17 14:17
 *@since:
 *@copyright: 版权所有2021 开源组织 gitee(https://gitee.com/jinzheyi)作者：朱述勇<br/>
 *            GitHub(https://github.com/jinzheyi)作者：朱述勇 。
 */
@Service
public class PlatformLogininforServiceImpl extends ServiceImpl<PlatformLogininforMapper, PlatformLogininfor>
        implements IPlatformLogininforService {

    @Autowired
    private PlatformLogininforMapper logininforMapper;

    /**
     * 查询系统登录日志集合
     *
     * @param logininfor 访问日志对象
     * @return 登录记录集合
     */
    @Override
    public List<PlatformLogininforVO> selectLogininforList(PlatformLogininforDTO logininfor)
    {
        return logininforMapper.selectLogininforList(logininfor);
    }

    /**
     * 批量删除系统登录日志
     *
     * @param ids 需要删除的数据
     * @return
     */
    @Override
    public int deleteLogininforByIds(String ids)
    {
        return logininforMapper.deleteBatchIds(Arrays.asList(Convert.toStrArray(ids)));
    }

    /**
     * 清空系统登录日志
     */
    @Override
    public void cleanLogininfor()
    {
        logininforMapper.cleanLogininfor();
    }

}
