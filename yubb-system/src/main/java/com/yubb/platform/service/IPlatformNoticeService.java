package com.yubb.platform.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yubb.common.core.page.TableDataInfo;
import com.yubb.platform.domain.PlatformNotice;
import com.yubb.platform.domain.dto.PlatformNoticeDTO;
import com.yubb.platform.domain.vo.PlatformNoticeVO;

import java.util.List;

/**
 *@Description 公告 服务层
 *@Author zhushuyong
 *@Date 2021/6/21 22:39
 *@since:
 *@copyright: 版权所有2021 开源组织 gitee(https://gitee.com/jinzheyi)作者：朱述勇<br/>
 *            GitHub(https://github.com/jinzheyi)作者：朱述勇 。
 */
public interface IPlatformNoticeService extends IService<PlatformNotice> {

    /**
     * 查询公告信息
     *
     * @param noticeId 公告ID
     * @return 公告信息
     */
    public PlatformNoticeVO selectNoticeById(String noticeId);

    /**
     * 查询公告列表
     *
     * @param notice 公告信息
     * @return 公告集合
     */
    public TableDataInfo<PlatformNoticeVO> selectNoticeList(PlatformNoticeDTO notice);

    /**
     * 新增公告
     *
     * @param notice 公告信息
     * @return 结果
     */
    public int insertNotice(PlatformNoticeDTO notice);

    /**
     * 修改公告
     *
     * @param notice 公告信息
     * @return 结果
     */
    public int updateNotice(PlatformNoticeDTO notice);

    /**
     * 删除公告信息
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteNoticeByIds(String ids);

}
