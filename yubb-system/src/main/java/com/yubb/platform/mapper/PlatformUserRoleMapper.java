package com.yubb.platform.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yubb.platform.domain.PlatformUserRole;
import org.apache.ibatis.annotations.Mapper;

/**
 * @Description 平台用户角色mapper
 * @Author zhushuyong
 * @Date 2021/7/15 22:29
 * @since:
 * @copyright: 版权所有2021 开源组织 gitee(https://gitee.com/jinzheyi)作者：朱述勇<br/>
 * GitHub(https://github.com/jinzheyi)作者：朱述勇 。
 */
@Mapper
public interface PlatformUserRoleMapper extends BaseMapper<PlatformUserRole> {
}
