package com.yubb.platform.mapper;

import java.util.List;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yubb.common.core.domain.platform.SysConfig;
import com.yubb.common.core.domain.platform.dto.SysConfigDTO;
import com.yubb.common.core.domain.platform.vo.SysConfigVO;
import org.apache.ibatis.annotations.Mapper;

/**
 *@Description 参数配置 数据层
 *@Author zhushuyong
 *@Date 2021/6/20 20:28
 *@since:
 *@copyright: 版权所有2021 开源组织 gitee(https://gitee.com/jinzheyi)作者：朱述勇<br/>
 *            GitHub(https://github.com/jinzheyi)作者：朱述勇 。
 */
@Mapper
public interface SysConfigMapper extends BaseMapper<SysConfig> {
    /**
     * 查询参数配置信息
     * 
     * @param config 参数配置信息
     * @return 参数配置信息
     */
    public SysConfigVO selectConfig(SysConfigDTO config);

    /**
     * 查询参数配置列表
     * 
     * @param config 参数配置信息
     * @return 参数配置集合
     */
    public List<SysConfigVO> selectConfigList(SysConfigDTO config);

}