package com.yubb.platform.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yubb.platform.domain.PlatformLogininfor;
import com.yubb.platform.domain.dto.PlatformLogininforDTO;
import com.yubb.platform.domain.vo.PlatformLogininforVO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 *@Description 系统访问日志情况信息 数据层
 *@Author zhushuyong
 *@Date 2021/6/17 14:16
 *@since:
 *@copyright: 版权所有2021 开源组织 gitee(https://gitee.com/jinzheyi)作者：朱述勇<br/>
 *            GitHub(https://github.com/jinzheyi)作者：朱述勇 。
 */
@Mapper
public interface PlatformLogininforMapper extends BaseMapper<PlatformLogininfor> {

    /**
     * 查询系统登录日志集合
     *
     * @param logininfor 访问日志对象
     * @return 登录记录集合
     */
    public List<PlatformLogininforVO> selectLogininforList(PlatformLogininforDTO logininfor);

    /**
     * 清空系统登录日志
     *
     * @return 结果
     */
    public int cleanLogininfor();

}
