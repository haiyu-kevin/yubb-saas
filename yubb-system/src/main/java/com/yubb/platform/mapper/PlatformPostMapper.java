package com.yubb.platform.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yubb.platform.domain.PlatformPost;
import com.yubb.platform.domain.vo.PlatformPostVO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 *@Description 岗位信息 数据层
 *@Author zhushuyong
 *@Date 2021/5/27 12:21
 *@since:
 *@copyright: 版权所有2021 开源组织 gitee(https://gitee.com/jinzheyi)作者：朱述勇<br/>
 *            GitHub(https://github.com/jinzheyi)作者：朱述勇 。
 */
@Mapper
public interface PlatformPostMapper extends BaseMapper<PlatformPost> {

    /**
     * 根据用户ID查询岗位
     *
     * @param userId 用户ID
     * @return 岗位列表
     */
    public List<PlatformPostVO> selectPostsByUserId(String userId);

}
