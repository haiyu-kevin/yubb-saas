package com.yubb.platform.mapper.saas;

import com.baomidou.mybatisplus.annotation.InterceptorIgnore;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yubb.system.domain.SysUserOnline;
import org.apache.ibatis.annotations.Mapper;

/**
 *@Description 在线用户 数据层
 *@Author zhushuyong
 *@Date 2021/6/19 22:37
 *@since:
 *@copyright: 版权所有2021 开源组织 gitee(https://gitee.com/jinzheyi)作者：朱述勇<br/>
 *            GitHub(https://github.com/jinzheyi)作者：朱述勇 。
 */
@Mapper
@InterceptorIgnore(tenantLine = "true")
public interface SaaSUserOnlineMapper extends BaseMapper<SysUserOnline> {

}
