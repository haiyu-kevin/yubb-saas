package com.yubb.platform.domain.dto;

import com.yubb.common.core.domain.platform.base.PlatformDTO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Data
@SuperBuilder(toBuilder = true)
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@ApiModel(value="岗位传入参数对象",description="岗位传入参数对象")
public class PlatformPostDTO extends PlatformDTO {

    private static final long serialVersionUID = 1L;

    @NotBlank(message = "岗位编码不能为空")
    @Size(min = 0, max = 64, message = "岗位编码长度不能超过64个字符")
    @ApiModelProperty(value = "岗位编码", required = true)
    private String postCode;

    @NotBlank(message = "岗位名称不能为空")
    @Size(min = 0, max = 50, message = "岗位名称长度不能超过50个字符")
    @ApiModelProperty(value = "岗位名称", required = true)
    private String postName;

    @NotBlank(message = "显示顺序不能为空")
    @ApiModelProperty(value = "岗位排序", required = true)
    private String postSort;

    @ApiModelProperty(value = "状态（0正常 1停用）", example = "0=正常,1=停用")
    private String status;

}
