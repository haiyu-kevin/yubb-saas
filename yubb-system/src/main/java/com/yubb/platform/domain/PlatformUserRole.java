package com.yubb.platform.domain;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 *@Description 用户和角色关联
 *@Author zhushuyong
 *@Date 2021/5/31 14:09
 *@since:
 *@copyright: 版权所有2021 开源组织 gitee(https://gitee.com/jinzheyi)作者：朱述勇<br/>
 *            GitHub(https://github.com/jinzheyi)作者：朱述勇 。
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@TableName("platform_user_role")
public class PlatformUserRole implements Serializable {

    /** 用户ID */
    private String userId;
    
    /** 角色ID */
    private String roleId;

}
