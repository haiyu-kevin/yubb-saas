package com.yubb.platform.domain;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 *@Description 系统访问记录表
 *@Author zhushuyong
 *@Date 2021/5/31 17:20
 *@since:
 *@copyright: 版权所有2021 开源组织 gitee(https://gitee.com/jinzheyi)作者：朱述勇<br/>
 *            GitHub(https://github.com/jinzheyi)作者：朱述勇 。
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("platform_logininfor")
public class PlatformLogininfor extends Model<PlatformLogininfor> {
    private static final long serialVersionUID = 1L;

    //指定主键生成使用雪花算法策略
    @TableId(type = IdType.ASSIGN_ID)
    @TableField(fill = FieldFill.INSERT)
    private String id;

    /** 用户账号 */
    private String loginName;

    /** 登录IP地址 */
    private String ipaddr;

    /** 登录地点 */
    private String loginLocation;

    /** 浏览器类型 */
    private String browser;

    /** 操作系统 */
    private String os;

    /** 登录状态 0成功 1失败 */
    private String status;

    /** 提示消息 */
    private String msg;

    /** 访问时间 */
    private Date loginTime;

}