package com.yubb.common.constant;

/**
 * 通用常量信息
 * 
 * @author ruoyi
 */
public class Constants
{
    /**
     * UTF-8 字符集
     */
    public static final String UTF8 = "UTF-8";

    /**
     * GBK 字符集
     */
    public static final String GBK = "GBK";

    /**
     * 通用成功标识
     */
    public static final String SUCCESS = "0";

    /**
     * 通用失败标识
     */
    public static final String FAIL = "1";

    /**
     * 登录成功
     */
    public static final String LOGIN_SUCCESS = "Success";

    /**
     * 注销
     */
    public static final String LOGOUT = "Logout";

    /**
     * 注册
     */
    public static final String REGISTER = "Register";

    /**
     * 登录失败
     */
    public static final String LOGIN_FAIL = "Error";

    /**
     * 当前记录起始索引
     */
    public static final String PAGE_NUM = "pageNum";

    /**
     * 每页显示记录数
     */
    public static final String PAGE_SIZE = "pageSize";

    /**
     * 排序列
     */
    public static final String ORDER_BY_COLUMN = "orderByColumn";

    /**
     * 排序的方向 "desc" 或者 "asc".
     */
    public static final String IS_ASC = "isAsc";
    
    /**
     * 系统用户授权缓存
     */
    public static final String SYS_AUTH_CACHE = "sys-authCache";

    /**
     * 平台用户授权缓存
     */
    public static final String PLATFORM_AUTH_CACHE = "platform-authCache";

    /**
     * 参数管理 cache name
     */
    public static final String SYS_CONFIG_CACHE = "sys-config";

    /**
     * 参数管理 cache key
     */
    public static final String SYS_CONFIG_KEY = "sys_config:";

    /**
     * 字典管理 cache name
     */
    public static final String SYS_DICT_CACHE = "sys-dict";

    /**
     * 字典管理 cache key
     */
    public static final String SYS_DICT_KEY = "sys_dict:";

    /**
     * 资源映射路径 前缀
     */
    public static final String RESOURCE_PREFIX = "/profile";

    /**
     * 正常标识
     */
    public static final String DEL_FLAG_NORMAL = "0";

    /**
     * 逻辑删除标识
     */
    public static final String DEL_FLAG_DEL = "2";

    /**
     * 父菜单id
     */
    public static final String PARENT_MENU_ID = "0";

    /**
     * 顶级父部门id
     */
    public static final String TOP_PARENT_DEPT_ID = "0";

    /**
     * 顶级父菜单id
     */
    public static final String TOP_PARENT_MENU_ID = "0";

    /**
     * 默认系统添加修改者
     */
    public static final String SYSTEM_USER_NAME = "yubb-system";

    /**
     * 平台登录
     */
    public static final String PLATFORM_LOGIN_URL = Constants.PLATFORM + "login";

    /**
     * 平台
     */
    public static final String PLATFORM = "/platform/";

    /**
     * 平台超管角色标识
     */
    public static final String PLATFORM_ADMIN = "platform_admin";

    /**
     * 平台超级管理员/租户超管用户标识
     */
    public static final String YB = "yb";

    /**
     * 租户超管角色标识
     */
    public static final String SAAS_ADMIN = "saas_admin";

    /**
     * 平台添加的租户角色
     */
    public static final String  PLATFORM_SAAS = "platform_saas";

    /**
     * 租户正常
     */
    public final static String STATUS_ZERO = "0";
    /**
     * 租户停用
     */
    public final static String STATUS_ONE = "1";

}
