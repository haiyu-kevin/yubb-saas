package com.yubb.common.core.domain.platform;

import com.baomidou.mybatisplus.annotation.TableName;
import com.yubb.common.core.domain.platform.base.PlatformDO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

/**
 *@Description 菜单权限表
 *@Author zhushuyong
 *@Date 2021/5/30 23:22
 *@since:
 *@copyright: 版权所有2021 开源组织 gitee(https://gitee.com/jinzheyi)作者：朱述勇<br/>
 *            GitHub(https://github.com/jinzheyi)作者：朱述勇 。
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder(toBuilder = true)
@EqualsAndHashCode(callSuper = true)
@TableName("sys_menu")
public class SysMenu extends PlatformDO {

    private static final long serialVersionUID = 1L;

    /** 菜单名称 */
    private String menuName;

    /** 父菜单ID */
    private String parentId;

    /** 显示顺序 */
    private String orderNum;

    /** 菜单URL */
    private String url;

    /** 打开方式（menuItem页签 menuBlank新窗口） */
    private String target;

    /** 类型（M目录 C菜单 F按钮） */
    private String menuType;

    /** 菜单使用类型
     00   基本菜单（租户拥有的最基础的菜单，租户永久拥有）
     01   免费赠送菜单（以订单收费为0的形式赠送，收回的话直接修改订单）
     02   付费菜单（租户购买之后，有效期之内可以任意使用） */
    private String menuCType;

    /** 菜单状态（0显示 1隐藏） */
    private String visible;

    /** 是否刷新（0刷新 1不刷新） */
    private String isRefresh;

    /** 权限字符串 */
    private String perms;

    /** 菜单图标 */
    private String icon;

    /** 备注 */
    private String remark;

}
