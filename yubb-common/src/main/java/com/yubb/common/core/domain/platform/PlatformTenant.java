package com.yubb.common.core.domain.platform;

import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableName;
import com.yubb.common.core.domain.platform.base.PlatformDO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

/**
 * 租户对象 platform_tenant
 * 
 * @author zhushuyong
 * @date 2021-07-02
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder(toBuilder = true)
@EqualsAndHashCode(callSuper = true)
@TableName("platform_tenant")
public class PlatformTenant extends PlatformDO {
    private static final long serialVersionUID = 1L;

    /** 租户名称 */
    private String tenantName;

    /** 租户编号 */
    private String tenantNo;

    /**
     * 租户logo
     */
    private String logoUrl;

    /** 租户状态（0正常 1停用） */
    private String status;

    /** 租户系统开始时间 */
    private Date startDate;

    /** 租户系统到期时间 */
    private Date endDate;

    /**
     * 备注
     */
    private String remark;

}
