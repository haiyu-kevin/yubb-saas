package com.yubb.common.core.domain.platform;

import com.baomidou.mybatisplus.annotation.TableName;
import com.yubb.common.core.domain.platform.base.PlatformDO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

/**
 *@Description 参数配置表
 *@Author zhushuyong
 *@Date 2021/5/31 13:13
 *@since:
 *@copyright: 版权所有2021 开源组织 gitee(https://gitee.com/jinzheyi)作者：朱述勇<br/>
 *            GitHub(https://github.com/jinzheyi)作者：朱述勇 。
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder(toBuilder = true)
@EqualsAndHashCode(callSuper = true)
@TableName("sys_config")
public class SysConfig extends PlatformDO {
    private static final long serialVersionUID = 1L;

    /** 参数名称 */
    private String configName;

    /** 参数键名 */
    private String configKey;

    /** 参数键值 */
    private String configValue;

    /** 系统内置（Y是 N否） */
    private String configType;

    /** 备注 */
    private String remark;

}