package com.yubb.common.exception.user;

/**
 *@Description 租户不在开放时间
 *@Author zhushuyong
 *@Date 2021/8/10 13:09
 *@since:
 *@copyright: 版权所有2021 开源组织 gitee(https://gitee.com/jinzheyi)作者：朱述勇<br/>
 *            GitHub(https://github.com/jinzheyi)作者：朱述勇 。
 */
public class UserTenantDateRangeException extends UserException
{
    private static final long serialVersionUID = 1L;

    public UserTenantDateRangeException()
    {
        super("user.tenant.date.range", null);
    }
}
