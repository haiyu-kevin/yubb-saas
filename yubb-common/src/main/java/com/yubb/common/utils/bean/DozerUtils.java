package com.yubb.common.utils.bean;

import com.github.dozermapper.core.DozerBeanMapperBuilder;
import com.github.dozermapper.core.Mapper;
import com.yubb.common.exception.BusinessException;
import java.util.*;

/**
 * @Description dozer是一个能把实体与实体之间转换的工具。类似spring的BeanUtils，但功能比BeanUtils强大，性能稍差。<br/>
 *              复杂的用DozerUtils，简单的可以继续用BeanUtils
 * @Author zhushuyong
 * @Date 2021/5/27 23:11
 * @since:
 * @copyright: 版权所有2021 开源组织 gitee(https://gitee.com/jinzheyi)作者：朱述勇<br/>
 * GitHub(https://github.com/jinzheyi)作者：朱述勇 。
 */
public class DozerUtils {

    static Mapper mapper = DozerBeanMapperBuilder.buildDefault();

    /**
     * 对象拷贝
     * @param source 源对象
     * @param targetClass 目标对象
     * @param <T> 泛型
     * @return 目标对象
     */
    public static <T> T copyProperties(Object source, Class<T> targetClass){
        if (Objects.isNull(source)){
            throw new BusinessException("copyProperties对象拷贝源对象为空");
        }
        return mapper.map(source,targetClass);
    }

}
