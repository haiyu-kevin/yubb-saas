package com.yubb.framework.handler;

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import com.yubb.common.constant.Constants;
import com.yubb.common.utils.ShiroUtils;
import com.yubb.common.utils.StringUtils;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.stereotype.Component;
import java.util.Date;

/**
 * @Description mybatis-plus自动填充字段
 * @Author zhushuyong
 * @Date 2021/6/24 12:58
 * @since:
 * @copyright: 版权所有2021 开源组织 gitee(https://gitee.com/jinzheyi)作者：朱述勇<br/>
 * GitHub(https://github.com/jinzheyi)作者：朱述勇 。
 */
@Component
public class MyMetaObjectHandler implements MetaObjectHandler {

    @Override
    public void insertFill(MetaObject metaObject) {
        String userName = StringUtils.isNull(ShiroUtils.getLoginName()) ? Constants.SYSTEM_USER_NAME : ShiroUtils.getLoginName();
        this.setFieldValByName("createTime",new Date(),metaObject);
        this.setFieldValByName("createBy", userName, metaObject);
        this.setFieldValByName("updateTime",new Date(),metaObject);
        this.setFieldValByName("updateBy", userName, metaObject);
    }

    @Override
    public void updateFill(MetaObject metaObject) {
        String userName = StringUtils.isNull(ShiroUtils.getLoginName()) ? Constants.SYSTEM_USER_NAME : ShiroUtils.getLoginName();
        this.setFieldValByName("updateTime",new Date(),metaObject);
        this.setFieldValByName("updateBy", userName, metaObject);
    }
}
