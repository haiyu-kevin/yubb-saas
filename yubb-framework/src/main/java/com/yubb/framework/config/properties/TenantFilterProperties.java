package com.yubb.framework.config.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
import java.util.List;

/**
 *@Description 租户过滤相关属性
 *@Author zhushuyong
 *@Date 2021/6/26 9:19
 *@since:
 *@copyright: 版权所有2021 开源组织 gitee(https://gitee.com/jinzheyi)作者：朱述勇<br/>
 *            GitHub(https://github.com/jinzheyi)作者：朱述勇 。
 */
@Data
@Component
@ConfigurationProperties(prefix = "tenant")
public class TenantFilterProperties {

    /**
     * 租户字段名
     */
    private String tenantIdColumn;

    /**
     * 租户需要过滤的表名
     */
    private List<String> tableFilter;

    /**
     * 租户需要过滤的方法
     */
    private List<String> tenantFilterMethod;

}
